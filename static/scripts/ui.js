var UI = {};

UI.initialize = function(){
    UI.buddy.initialize();
    UI.login.initialize();
    UI.interact.initialize();
    UI.notifier.initialize();

    $('#changePwdForm').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
    });

    $('#changePwdForm').dialog('option', 'buttons', [{
        click: function(){ alert('尚未制作，请联系作者。'); },
        text: '修改密码',
    }]);
    
    $('#jsDisableNotice').css({'display':'none'});
    $('button').button();

    $('#addTest2').click( function(){
    });

};

/******************************************************************************/
UI.buddy = {};
UI.buddy.initialize = function(){    
    $('#buddyAddButton').click( function(){UI.buddy.addBuddy();} );
    $('#contactFormList').accordion({
        heightStyle: 'fill',
    });
    $('#refreshBuddyList').click( function(){userData.refresh();} );
    $('#contactForm').dialog({
        autoOpen: false,
        modal: false,
        resizable: true,
        resize: function(){ $('#contactFormList').accordion('refresh'); },
        minHeight: 400,
        minWidth: 300,
        position: { my: 'left top', at: 'left+50 top+100', of: 'body' },
    });
}
UI.buddy.refresh = function(){
    // Called by userData.refresh
    $('#buddyMenuBox ul#menu').remove();

    $('<ul>',{
        id: 'menu',
    }).appendTo('#buddyMenuBox');

    for(userid in userData.names){
        $('<li>').appendTo('#buddyList ul#menu')
                 .append($('<a>',{
                            href: '#'
                         }).text(userData.names[userid])
                           .bind('click', userid, function(e){ new dialog(e.data).show(); } )
                         );
    }
    $('#buddyList ul#menu').menu();

    $('#contactForm').dialog('open');
    $('#contactFormList').accordion('refresh');
}
UI.buddy.addBuddy = function(){
    $.post('queryuser.php',{'token':token,'query':$('#buddyAdd input[name=username]').val()},
            function(j){
                if(j.type != 'success'){
                    UI.interact.alert('查找好友失败。');
                } else {
                    $.post('buddy.php?action=add',{'token':token, 'id':j.data.id},function(j){
                        if(j.type == 'success'){
                            UI.interact.alert('添加好友成功。');
                            userData.refresh();
                        } else {
                            UI.interact.alert('添加好友失败。');
                        }
                    },'json');
                }
            },'json');
}

/******************************************************************************/
UI.login = {};
UI.login.initialize = function(){    
    $('#loginForm').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
    });

    $("#loginFormTabs").tabs({
        activate: UI.login.setDialogButton,
    });

    UI.login.setDialogButton();
}
UI.login.doLogin = function(){
    loginDo($('#loginFormTabs-1 input[name=username]').val(), $('#loginFormTabs-1 input[name=password]').val());
}
UI.login.doReg = function(){
    var username = $('#loginFormTabs-2 input[name=username]').val();
    var password1 = $('#loginFormTabs-2 input[name=password]').val();
    var password2 = $('#loginFormTabs-2 input[name=password2]').val();

    if(password1 != password2){
        UI.login.setRegInfo('两次密码不一致','error');
        return;
    }
    doReg(username,password1);
}
UI.login.setDialogButton = function(){
    var active = $('#loginFormTabs').tabs('option','active');
    if(active == 0){
        $('#loginForm').dialog('option','buttons',[{
            click: UI.login.doLogin,
            text: '登录'
        }]);
    } else if(active == 1) {
        $('#loginForm').dialog('option','buttons',[{
            click: UI.login.doReg,
            text: '注册',
        }]);
    }
}
UI.login.setLoginInfo = function(info,type){
    $('#loginFormTabs-1 div[name=message]')
        .html(info)
        .removeAttr('style')
        .addClass('ui-state-' + type);
}
UI.login.setRegInfo = function(info,type){
    $('#loginFormTabs-2 div[name=message]')
        .html(info)
        .removeAttr('style')
        .addClass('ui-state-' + type);
}

/******************************************************************************/
UI.interact = {}
UI.interact.initialize = function(){
    $('#interactAlert').dialog({
        autoOpen: false,        
        buttons: [{ text: '确认', click: function(){ $(this).dialog('close'); }, },],
        modal: true,
    });
}
UI.interact.alert = function(message){
    $('#interactAlert')
        .text(message)
        .dialog('open');
}

/******************************************************************************/
UI.notifier = {}
UI.notifier.initialize = function(){
    UI.notifier.setHumanActivity();
    $('body').bind('keydown keyup keypress mousedown mouseenter mouseleave mousemove mouseout mouseover mouseup click dblclick',
                   UI.notifier.setHumanActivity);
    $('body').data('originalTitle',$(document).attr('title'));
    $('body').data('titleInAnimate', true);
    $('body').data('animateTitle',false);
    UI.notifier.animate();
}
UI.notifier.notify = function(){
    var nowtime = new Date().getTime();
    var lastsee = $('body').data('lastHumanActivity');
    if(nowtime - lastsee > 8000){
        $("#audio")[0].play();
        $('body').data('animateTitle',true);
    }
}
UI.notifier.setHumanActivity = function(){
    $('body').data('lastHumanActivity', new Date().getTime());
    if($('body').data('animateTitle')){
        $('body').data('animateTitle',false);
        $(document).attr('title', $('body').data('originalTitle'));
    }
}
UI.notifier.animate = function(){
    if($('body').data('animateTitle') != false){
        $('body').data('titleInAnimate', !($('body').data('titleInAnimate')));
        if( $('body').data('titleInAnimate') )
            $(document).attr('title', '【　　　】' + $('body').data('originalTitle'));
        else
            $(document).attr('title', '【新消息】' + $('body').data('originalTitle'));
    }
    setTimeout(UI.notifier.animate, 800);
}
