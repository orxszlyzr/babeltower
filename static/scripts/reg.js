function doReg(username,password){
    var checkhash = (new jsSHA(username + password, "TEXT")).getHash("SHA-1","HEX");
    $.post('register.php',{ 'username':username, 'password':password, 'check':checkhash },
            function(j){
                $('#loginForm input[name=password]').val('');
                if(j.type == null)
                    return;
                if(j.type != 'success'){
                    var desc = '';
                    if(j.description != null)
                        desc = j.description;
                    else if(j.error != null)
                        desc = j.error.message;
                    else
                        desc = '未知错误';
                    UI.login.setRegInfo(desc,'error');
                } else {
                    UI.login.setRegInfo('注册成功，请登录。','highlight');
                }
            },
           'json');
}
