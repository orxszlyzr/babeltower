<?
include(dirname(__FILE__) . "/_general_.php");

$tokenstr = request_var('token');
$queryname = request_var('query'); 

if(!$tokenstr)
    $r = new failure('token not present.');
else {
    $token = new token();
    if(!$result = $token->read($tokenstr))
        $r = new failure('token invalid.');
    else {
        $u = new userManager();
        $result = $u->userExists($queryname);
        if($result === false){
            $r = new failure('no such user.');
        } else {
            $r = new success(array('id'=>$result['id'],
                                   'name'=>base64_decode($result['username']),
                            ));
        }
    }
}
if(isset($r)) die($r->getJSON());
