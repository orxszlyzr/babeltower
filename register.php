<?
include(dirname(__FILE__) . "/_general_.php");

$username = request_var('username');
$password = request_var('password');
$check = request_var('check');

if(!($username && $password && $check))
    $r = new failure('data not sufficient');
else {
    if($check != sha1($username . $password))
        $r = new failure('detected transmission error. try again.');
    else {
        $u = new userManager();
        $result = $u->userNew($username,$password);
        if($result === true)
            $r = new success('registered.');
        else if($result == -1)
            $r = new failure('invalid username.');
        else if($result == -2)
            $r = new failure('user exists.');
        else
            $r = new failure('unknown error.');
    }
}

if(isset($r)) die($r->getJSON());
